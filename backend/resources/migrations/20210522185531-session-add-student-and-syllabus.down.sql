ALTER TABLE sessions ADD COLUMN progress_id INTEGER;
--;;
UPDATE sessions as s
SET progress_id = p.id
FROM progress p
WHERE s.student_id = p.student_id
  AND s.syllabus_id = p.syllabus_id;
--;;
ALTER TABLE sessions DROP COLUMN student_id;
--;;
ALTER TABLE sessions DROP COLUMN syllabus_id;