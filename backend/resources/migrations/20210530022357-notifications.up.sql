CREATE TABLE notifications (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	date TEXT,
	student_id INTEGER,
	user_id INTEGER,
	message TEXT
);


