CREATE TABLE IF NOT EXISTS progress (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    student_id INTEGER,
    syllabus_id INTEGER,
    pending INTEGER,
    inprogress INTEGER,
    learned INTEGER);