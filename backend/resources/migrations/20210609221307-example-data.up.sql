INSERT INTO users (id, name, mail, password)
VALUES
	(1, 'Antonio', 'admin1@admin.com', '$s0$f0801$dXDq+fXYN4cZ/A2jAIyK6g==$90QHvI6gnRxDuvRs06SrYUcbAWqYDUeO8Lh0b2Y2lgQ='),
	(2, 'Beatriz', 'admin2@admin.com', '$s0$f0801$dXDq+fXYN4cZ/A2jAIyK6g==$90QHvI6gnRxDuvRs06SrYUcbAWqYDUeO8Lh0b2Y2lgQ='),
	(3, 'Carlos', 'admin3@admin.com', '$s0$f0801$dXDq+fXYN4cZ/A2jAIyK6g==$90QHvI6gnRxDuvRs06SrYUcbAWqYDUeO8Lh0b2Y2lgQ=');

--;;

INSERT INTO students (id, name, surname, inscription, phone, mail, state)
VALUES
	(1, 'Alvaro', 'Arrimadas', '2021-02-06', '666777888', 'alvaro@prueba.com', 'ACTIVO'),
	(2, 'Benito', 'Basallote', '2021-02-25', '666777888', 'beniteo@prueba.com', 'ACTIVO'),
	(3, 'Carlos', 'Caronte', '2021-05-01', '666777888', NULL, 'DESACTIVADO');

--;;

INSERT INTO syllabus (id, title, content, parent, "level", pos)
VALUES
	(1, 'TEMA A', 'Contenido A', NULL, 0, 1),
	(2, 'TEMA B', 'Contenido B', NULL, 0, 2),
	(3, 'TEMA C', 'Contenido C', NULL, 0, 3);

--;;

INSERT INTO cards (id, title, syllabus_id, question, answer)
VALUES
	(1, 'tarjeta1', 1, 'pregunta 1', 'respuesta 1'),
	(2, 'tarjeta2', 1, 'pregunta 2', 'respuesta 2'),
	(3, 'tarjeta3', 1, 'pregunta 3', 'respuesta 3');
