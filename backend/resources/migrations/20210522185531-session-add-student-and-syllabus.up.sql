ALTER TABLE sessions ADD student_id INTEGER;
--;;
ALTER TABLE sessions ADD syllabus_id INTEGER;
--;;
UPDATE sessions as s
SET student_id = p.student_id,
    syllabus_id = p.syllabus_id
FROM progress p
WHERE s.progress_id = p.id;
--;;
ALTER TABLE sessions DROP COLUMN progress_id;
