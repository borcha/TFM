CREATE TABLE IF NOT EXISTS "students" (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	name TEXT,
	surname TEXT,
	inscription TEXT,
	phone TEXT,
	mail TEXT,
	state TEXT,
	code TEXT
);