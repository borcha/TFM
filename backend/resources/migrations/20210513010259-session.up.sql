CREATE TABLE IF NOT EXISTS sessions (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	progress_id INTEGER,
	date TEXT,
	duration INTEGER,
	stats json
);
