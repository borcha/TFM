(defproject kaeme "0.1.0-SNAPSHOT"
  :description "Server Side Public License (SSPL)"
  :license {:name "GNU Affero General Public License v3.0"
            :url "https://www.gnu.org/licenses/agpl-3.0.en.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/tools.namespace "1.1.0"]
                 [mount "0.1.16"]
                 [environ "1.2.0"]
                 [com.walmartlabs/lacinia-pedestal "0.15.0"]
                 [org.clojure/java.jdbc "0.7.8"]
                 [io.github.willena/sqlite-jdbc "3.35.3"]
                 [com.layerware/hugsql "0.5.1"]
                 [migratus "1.3.5"]
                 [cheshire "5.10.0"]
                 [crypto-password "0.2.1"]
                 [io.aviso/logging "0.3.1"]]
  :plugins [[lein-environ "1.2.0"]]
  :main kaeme.system
  :resource-paths ["resources" "../frontend/dist"]
  :target-path "target/%s/"
  :env { :database "../db/kaeme.db"}
  :profiles {:uberjar {:aot :all
                       :uberjar-exclusions [#"^kaeme/.*\.clj"]
                       :uberjar-name "kaeme.jar"}})