(ns kaeme.system
  (:gen-class)
  (:require
    [mount.core :as mount]
    [kaeme.schema :as schema]
    [kaeme.server :as server]
    [kaeme.db :as db]))

(defn -main []
  (println "Iniciando ...")
  (mount/start))
