(ns kaeme.progress.leitner)

(defn refresh-progress
  "Actualiza el progreso eliminando y añadiendo nuevas tarjetas que se hayan creado"
  [progress current-ids]
  (let [current (set current-ids)
        valid   (filter #(current (:card %)) progress)
        valid-ids (set (map :card valid))
        new     (map #(hash-map :card % :level 0) (remove valid-ids current))]
    (doall (into valid new))))

(defn- level-up [x] (min (inc x) 4))

(defn- level-down [x] (max (dec x) 0))

(defn update-progress
  "Recibe el progreso anterior y el resultado de una sesión.
  Devuelve la lista de cards subiendo o bajando de nivel cada tarjeta."
  [progress session]
  (let [hit   (set (:hits session))
        fault (set (:faults session))]
    (map (fn [{id :card :as card}]
           (cond-> card
             (hit id)   (update :level level-up)
             (fault id) (update :level level-down)))
         progress)))

; https://stackoverflow.com/a/15823098
(defn- wrand
  "given a vector of slice sizes, returns the index of a slice given a
  random spin of a roulette wheel with compartments proportional to
  slices."
  [slices]
  (let [total (reduce + slices)
        r (rand total)]
    (loop [i 0 sum 0]
      (if (< r (+ (slices i) sum))
        i
        (recur (inc i) (+ (slices i) sum))))))

(def ^:private fibo-weigths [8 5 3 2 1])

(defn choose-cards
  "Elige n elementos de forma aleatoria siguiendo la distribución de pesos
  indicada por fibo-weigths por cada nivel de aprendizaje"
  [n cards]
  (loop [selected '() cards cards]
    (if (or (empty? cards)
            (= (count selected) n))
      selected
      (let [levels  (into (sorted-map) (group-by :level cards))
            weights (mapv fibo-weigths (keys levels))
            level   (nth (vals levels) (wrand weights))
            card    (rand-nth level)]
        (recur (conj selected card)
               (remove #{card} cards))))))
