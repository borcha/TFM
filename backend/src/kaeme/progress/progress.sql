-- Contar tarjetas
-- :name total-progress :? :1
SELECT SUM(pending) pending,
       SUM(inprogress) inprogress,
       SUM(learned) learned
  FROM progress p 
 WHERE student_id = :id
 GROUP BY student_id

-- Obtener el progreso de un punto del temario
-- :name get-progress-by-id :? :1
SELECT *
  FROM progress
 WHERE id = :id


-- Obtener la última sesión realizada
-- :name last-progress-of-student :? :1
SELECT *
  FROM progress
 WHERE student_id = :id
 ORDER BY date DESC
 LIMIT 1


-- Obtener el progreso del capítulo
-- :name chapter-progress-by-id-raw :? :1
SELECT *
  FROM progress
 WHERE syllabus_id = :syllabus_id
   AND student_id  = :student_id


-- Obtener la actividad de sesiones realizadas en el último mes
-- :name last-month-activity :? :*
WITH RECURSIVE days (day) AS (
  SELECT date('now', '-1 month', 'start of month', '-7 day', 'weekday 1') day
  UNION ALL
  SELECT date(day, '1 day') FROM days WHERE day < date('now')
),

user_sessions AS (
  SELECT strftime('%Y-%m-%d', date) date
    FROM sessions
   WHERE student_id = :id
     AND date >= (SELECT MIN(day) FROM days)
)

SELECT d.day, COUNT(s.date) total
  FROM days d
  LEFT JOIN user_sessions s
    ON d.day = s.date
 GROUP BY d.day
 ORDER BY d.day

-- Crear progreso
-- :name insert-progress :<!
INSERT INTO progress (student_id, syllabus_id, pending, inprogress, learned, date)
VALUES (:student_id, :syllabus_id, 0, 0, 0, datetime()) RETURNING *

-- Insertar sesión
-- :name insert-session-raw :<!
INSERT INTO sessions (student_id, syllabus_id, date, duration, stats)
VALUES(:student_id, :syllabus_id, :date, :duration, :stats) RETURNING *

-- Actualiza progreso
-- :name update-progress-raw :! :1
UPDATE progress
SET pending = :pending,
    inprogress = :inprogress,
    learned = :learned,
    date = :date,
    data = :data
WHERE id = :id

-- Eliminar progreso
-- :name delete-chapter-progress :! :n
DELETE FROM progress
 WHERE syllabus_id = :syllabus_id
   AND student_id  = :student_id