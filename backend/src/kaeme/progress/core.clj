(ns kaeme.progress.core
  (:use kaeme.macros)
  (:require
    [clojure.java.io :as io]
    [io.pedestal.log :as log]
    [hugsql.core :as hugsql]
    [clojure.java.jdbc :as jdbc]
    [clojure.java.jdbc :refer [with-db-transaction]]
    [clojure.string :as str]
    [kaeme.progress.db :as db]
    [kaeme.syllabus.db :as syllabus]
    [kaeme.cards.db :as cards]
    [kaeme.notifications.db :refer [add-notification]]
    [kaeme.progress.leitner :as leitner]))

(defn student-total-progress [db ctx args student]
  (db/total-progress db student))

(defn student-last-chapter [db ctx args student]
  (let [progress (db/last-progress-of-student db student)]
    (or (syllabus/syllabus-by-id db {:id (:syllabus_id progress)})
        (syllabus/syllabus-by db {:pos 1}))))

(defn student-activity [db ctx args student]
  (->> (db/last-month-activity db student)
       (mapv :total)
       (hash-map :sessions)))

(defn chapter-progress [db ctx args chapter]
  (let [user-info (:user-info ctx)
        syllabus_id (:id chapter)
        student_id  (or (and (contains? user-info :teacher) (:id args))
                        (:student user-info))]
    (db/chapter-progress-by-id db student_id syllabus_id)))

(defn- refresh-progress-data [db {:keys [syllabus_id data]}]
  (let [current-ids   (cards/all-cards-ids db {:syllabus_id syllabus_id})]
    (leitner/refresh-progress data current-ids)))

(defn get-session [db {user-info :user-info} {chapter :chapter} _]
  (let [student_id    (:student user-info)
        progress      (or (db/chapter-progress-by-id db student_id chapter)
                          (hash-map :student_id student_id :syllabus_id chapter :data []))
        progress-data (refresh-progress-data db progress)
        cards-ids     (map :card (leitner/choose-cards 10 progress-data))]
    (cards/all-cards db {:ids cards-ids})))

(defn- update-progress-from-session
  [db {:keys [student_id syllabus_id] :as progress} session]
  (let [progress-data (refresh-progress-data db progress)
        progress-data (leitner/update-progress progress-data session)
        pending       (count (filter #(=  0 (:level %)) progress-data))
        learned       (count (filter #(<= 3 (:level %)) progress-data))
        inprogress    (- (count progress-data) pending learned)
        params        (-> progress
                          (assoc :pending pending)
                          (assoc :learned learned)
                          (assoc :inprogress inprogress)
                          (assoc :date (:date session))
                          (assoc :data progress-data))]
    (db/update-progress db params)))

(defn create-session [db {user-info :user-info} {:keys [chapter data]} _]
  (let [student_id (:student user-info)
        syllabus_id chapter
        progress   (db/get-or-create-progress db student_id syllabus_id)
        session    (db/insert-session db student_id syllabus_id data)]
    (update-progress-from-session db progress session)
    (db/chapter-progress-by-id db student_id syllabus_id)))

(defn delete-progress [db ctx {syllabus_id :chapter} _]
  (with-db-transaction [tx db]
    (when-let* [student_id (get-in ctx [:user-info :student])
                chapter    (syllabus/syllabus-by-id tx {:id syllabus_id})]
      (add-notification tx nil student_id (format "Se elimina progreso %s" (:title chapter)))
      (= 1 (db/delete-chapter-progress tx {:student_id student_id :syllabus_id syllabus_id})))))

(def resolver-map
  {:Student/progress student-total-progress
   :Student/lastChapter student-last-chapter
   :Student/activity student-activity
   :Chapter/progress chapter-progress
   :Query/GetSession get-session
   :Mutation/CreateSession create-session
   :Mutation/DeleteProgress delete-progress})

(def schema (slurp (io/resource "kaeme/progress/progress.graphql")))
