(ns kaeme.progress.db
  (:require [hugsql.core :as hugsql]
            [io.pedestal.log :as log]
            [cheshire.core :as ch]))

(hugsql/def-db-fns "kaeme/progress/progress.sql")


(defn chapter-progress-by-id [db student_id syllabus_id]
  (when-let [progress (chapter-progress-by-id-raw db {:student_id student_id :syllabus_id syllabus_id})]
    (update progress :data #(ch/parse-string % true))))

(defn get-or-create-progress [db student_id syllabus_id]
  (or (chapter-progress-by-id db student_id syllabus_id)
      (first (insert-progress db {:student_id student_id :syllabus_id syllabus_id}))))

(defn insert-session [db student_id syllabus_id {:keys [hits faults] :as data}]
  (let [stats (ch/generate-string {:hits hits :faults faults})
        params (-> data
                   (assoc :student_id student_id)
                   (assoc :syllabus_id syllabus_id)
                   (assoc :stats stats))]
    (-> (insert-session-raw db params)
        first
        (assoc :hits hits)
        (assoc :faults faults))))

(defn update-progress [db params]
  (->> (update params :data ch/generate-string)
       (update-progress-raw db)))