(ns kaeme.cards.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "kaeme/cards/cards.sql")

(defn all-cards-ids [db params]
  (map :id (all-cards-ids-raw db params)))