-- Contar tarjetas
-- :name total-cards :? :1
SELECT COUNT(*) "total-cards"
FROM cards
WHERE syllabus_id = :id

-- Obtener tarjetas
-- :name all-cards :? :*
select * from cards
--~ (if (int? (:syllabus params)) "where syllabus_id = :syllabus")
--~ (if (contains? params :ids) "where id in (:v*:ids)")

-- Obtener ids de las tarjetas asociadas a un capítulo
-- :name all-cards-ids-raw :? :*
SELECT id
  FROM cards
 WHERE syllabus_id = :syllabus_id

-- Obtener una tarjeta
-- :name card-by-id :? :1
select * from cards where id = :id

-- Eliminar una tarjeta
-- :name delete-card-by-id :! :n
delete from cards where id = :id

-- Insertar una tarjeta
-- :name insert-card :<!
INSERT INTO cards (title, syllabus_id, question, answer)
VALUES(:title, :syllabus_id, :question, :answer) RETURNING *

-- Editar una tarjeta
-- :name update-card-by-id :! :1
UPDATE cards
SET title       = :title,
    syllabus_id = :syllabus_id,
    question    = :question,
    answer      = :answer
WHERE id = :id
