(ns kaeme.cards.core
  (:use kaeme.macros)
  (:require
    [clojure.java.io :as io]
    [io.pedestal.log :as log]
    [hugsql.core :as hugsql]
    [clojure.java.jdbc :as jdbc]
    [clojure.java.jdbc :refer [with-db-transaction]]
    [clojure.string :as str]
    [kaeme.cards.db :as db]
    [kaeme.notifications.db :refer [add-notification]]))

(defn all-cards [db _ args _]
  (db/all-cards db args))

(defn chapter-total-cards [db _ args chapter]
  (:total-cards (db/total-cards db chapter)))

(defn get-card [db _ args _]
  (db/card-by-id db args))

(defn delete-card [db ctx {id :id} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                card    (db/card-by-id tx {:id id})]
      (db/delete-card-by-id tx {:id id})
      (add-notification tx teacher (format "Se elimina tarjeta %d-%s" id (:title card)))
      card)))

(defn create-card [db ctx args _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                new-id  (db/insert-card tx (:data args))
                card    (db/card-by-id tx (first new-id))]
      (add-notification tx teacher (format "Se crea tarjeta %d-%s" (:id card) (:title card)))
      card)))

(defn update-card [db ctx args _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                db-args (merge (:data args) {:id (:id args)})
                _       (db/update-card-by-id db db-args)
                card    (db/card-by-id db args)]
      (add-notification tx teacher (format "Se modifica tarjeta %d-%s" (:id card) (:title card)))
      card)))

(def resolver-map
  {:Chapter/totalCards chapter-total-cards
   :Query/Cards all-cards
   :Query/Card get-card
   :Mutation/DeleteCard delete-card
   :Mutation/CreateCard create-card
   :Mutation/UpdateCard update-card})

(def schema (slurp (io/resource "kaeme/cards/cards.graphql")))
