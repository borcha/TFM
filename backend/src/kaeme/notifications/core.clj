(ns kaeme.notifications.core
  (:require
    [clojure.java.io :as io]
    [io.pedestal.log :as log]
    [hugsql.core :as hugsql]
    [clojure.java.jdbc :as jdbc]
    [clojure.string :as str]
    [kaeme.notifications.db :as db]))

(defn get-notifications [db {user-info :user-info} {:keys [id limit]} _]
  (when (contains? user-info :teacher)
    (db/get-notifications db {:student_id id :limit limit})))

(def resolver-map
  {:Query/Notifications get-notifications})

(def schema (slurp (io/resource "kaeme/notifications/notifications.graphql")))
