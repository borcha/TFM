-- Obtener listado de notificacions
-- :name get-notifications :? :*
SELECT n.*, u.name user_name, s.name || ' ' || s.surname student_name
FROM notifications n
LEFT JOIN users u ON n.user_id = u.id
LEFT JOIN students s ON n.student_id = s.id
--~ (when (some? (params :student_id)) "WHERE student_id = :student_id")
ORDER BY date DESC
LIMIT :limit

-- Insertar notificacion
-- :name insert-notification :<!
INSERT INTO notifications (date, student_id, user_id, message)
VALUES(datetime(), :student_id, :user_id, :message) RETURNING *
