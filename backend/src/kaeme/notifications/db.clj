(ns kaeme.notifications.db
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "kaeme/notifications/notifications.sql")

(defn add-notification
  ([db user_id message]
   (add-notification db user_id nil message))

  ([db user_id student_id message]
   (insert-notification db {:user_id user_id
                            :student_id student_id
                            :message message})))

; (defn add-notification-session [db user_id]
;   (let [sessions ()]))
; Obtener el númro de sesiones en el dia actual
; Si