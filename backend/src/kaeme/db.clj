(ns kaeme.db
  (:require
    [mount.core :refer [defstate]]
    [io.pedestal.log :as log]
    [hugsql.core :as hugsql]
    [environ.core :refer [env]]
    [clojure.java.jdbc :as jdbc]
    [migratus.core :as migratus]
    [clojure.string :as str]))

(defstate database
  :start { :classname   "org.sqlite.JDBC"
           :subprotocol "sqlite"
           :subname     (env :database "/app/db/kaeme.db")})

(defstate migrations
  :start (let [migratus-config {:store :database
                                :db database}]
           (migratus/migrate migratus-config)
           migratus-config))