-- Corregir niveles y orden según la jerarquía
-- :name recalcule-syllabus :! :1
with recursive items (lvl, id, title, pos) as (
  select 0 as lvl, id, title, ifnull(pos, 999)
  from syllabus
  where parent is null
  union all
  select items.lvl + 1, item.id, item.title, ifnull(item.pos, 999)
  from items
  join syllabus item on items.id = item.parent
  order by items.lvl+1 desc, 4
),

correct as (
  select id, lvl, ROW_NUMBER() OVER() pos
  from items i
)

UPDATE syllabus AS S
SET "level" = C.LVL, POS = C.POS
FROM correct c
WHERE s.id = c.id
  AND (s."level" IS NULL OR s."level" != c.lvl
       OR s.pos IS NULL OR s.pos != c.pos)

-- Obtiene los elementos hijos de un nodo
-- :name childs-of-syllabus :? :*
with recursive items (id) as (
  select id
  from syllabus
  where id = :id
  union all
  select item.id
  from items
  join syllabus item on items.id = item.parent
)

select id
from items

-- Obtener temario ordenado
-- :name all-syllabus :? :*
select * from syllabus order by pos

-- Obtener un capítulo
-- :name syllabus-by :? :1
select * from syllabus where
--~ (when (contains? params :id) "id = :id")
--~ (when (contains? params :pos) "pos = :pos")

-- Obtener un capítulo
-- :name syllabus-by-id :? :1
select * from syllabus where id = :id

-- Eliminar un capítulo
-- :name delete-syllabus-by-id :! :n
delete from syllabus where id = :id

-- Insertar un capítulo
-- :name insert-syllabus :<!
INSERT INTO syllabus (title, content, parent)
VALUES(:title, :content, :parent) RETURNING *

-- Editar un capítulo
-- :name update-syllabus-by-id-raw :! :1
UPDATE syllabus 
SET title = :title
--~ (when (contains? params :content) ",content = :content")
--~ (when (contains? params :parent) ",parent = :parent")
WHERE id = :id


-- Reorganizar temario
-- :name syllabus-move-up :! :1
UPDATE syllabus 
SET pos = IFNULL((SELECT pos - 0.5
                    FROM syllabus
                   WHERE level = :level
                     AND pos < :pos
                   ORDER BY pos DESC
                   LIMIT 1), pos)
WHERE id = :id

-- Reorganizar temario
-- :name syllabus-move-down :! :1
UPDATE syllabus 
SET pos = IFNULL((SELECT pos + 0.5
                    FROM syllabus
                   WHERE level = :level
                     AND pos > :pos
                   ORDER BY pos ASC
                   LIMIT 1), pos)
WHERE id = :id

-- Reorganizar temario
-- :name syllabus-move-left :! :1
UPDATE syllabus 
SET parent = (SELECT parent FROM syllabus WHERE id = :parent)
WHERE id = :id

-- Reorganizar temario
-- :name syllabus-move-right :! :1
UPDATE syllabus 
SET parent = IFNULL((SELECT id
                    FROM syllabus
                   WHERE (:parent IS NULL OR parent = :parent)
                     AND level = :level
                     AND pos < :pos
                   ORDER BY pos DESC
                   LIMIT 1), parent)
WHERE id = :id

