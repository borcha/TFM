(ns kaeme.syllabus.db
  (:require [hugsql.core :as hugsql]
            [io.pedestal.log :as log]))

(hugsql/def-db-fns "kaeme/syllabus/syllabus.sql")

(defn update-syllabus-by-id [db {:keys [id parent] :as params}]
  (let [childs (->> (childs-of-syllabus db params)
                    (map :id)
                    set)
        params (if (childs parent) ; No se permite elegir como padre a un descendiente
                 (dissoc params :parent)
                 params)]
    (update-syllabus-by-id-raw db params)))