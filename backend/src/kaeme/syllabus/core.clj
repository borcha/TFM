(ns kaeme.syllabus.core
  (:use kaeme.macros)
  (:require
    [clojure.java.io :as io]
    [clojure.java.jdbc :refer [with-db-transaction]]
    [io.pedestal.log :as log]
    [hugsql.core :as hugsql]
    [clojure.java.jdbc :as jdbc]
    [clojure.string :as str]
    [kaeme.notifications.db :refer [add-notification]]
    [kaeme.syllabus.db :as db]))

(defn all-syllabus [db _ args _]
  (db/all-syllabus db args))

(defn get-chapter [db _ args _]
  (db/syllabus-by db args))

(defn delete-chapter [db ctx {id :id} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                chapter (db/syllabus-by-id tx {:id id})]
      (db/delete-syllabus-by-id tx {:id id})
      (add-notification tx teacher (format "Se elimina capítulo %s" (:title chapter)))
      chapter)))

(defn create-chapter [db ctx args _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                data    (merge {:parent nil :content nil} (:data args))
                chapter (first (db/insert-syllabus tx data))]
      (db/recalcule-syllabus tx)
      (add-notification tx teacher (format "Se crea nuevo capítulo %s" (:title chapter)))
      (db/syllabus-by-id tx chapter))))

(defn update-chapter [db ctx args _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                db-args (merge (:data args) {:id (:id args)})]
      (db/update-syllabus-by-id tx db-args)
      (db/recalcule-syllabus tx)
      (add-notification tx teacher (format "Se modifica capítulo %s" (:title db-args)))
      (db/syllabus-by-id tx args))))

(defn is-last? [db _ _ {pos :pos}]
  (nil? (db/syllabus-by db {:pos (inc pos)})))

(defn reorder-chapter [db _ {:keys [id operation]} _]
  (with-db-transaction [tx db]
    (let [chapter (db/syllabus-by-id tx {:id id})]
      (case operation
        :UP    (db/syllabus-move-up tx chapter)
        :DOWN  (db/syllabus-move-down tx chapter)
        :LEFT  (db/syllabus-move-left tx chapter)
        :RIGHT (db/syllabus-move-right tx chapter))
      (db/recalcule-syllabus tx)
      (db/all-syllabus tx))))

(def resolver-map
  {:Chapter/last is-last?
   :Query/Syllabus all-syllabus
   :Query/Chapter get-chapter
   :Mutation/DeleteChapter delete-chapter
   :Mutation/CreateChapter create-chapter
   :Mutation/UpdateChapter update-chapter
   :Mutation/ReorderChapter reorder-chapter})

(def schema (slurp (io/resource "kaeme/syllabus/syllabus.graphql")))
