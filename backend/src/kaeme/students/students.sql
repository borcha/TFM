-- Obtener listado de alumnos
-- :name all-students :? :*
select * from students
--~ (if (seq (:order params)) "order by :i:order")

-- Obtener listado de alumnos
-- :name student-by-id :? :1
select * from students where id = :id

-- Eliminar alumno
-- :name delete-student-by-id :! :n
delete from students where id = :id

-- Insertar alumno
-- :name insert-student :<!
INSERT INTO students (name, surname, inscription, phone, mail, state)
VALUES(:name, :surname, :inscription, :phone, :mail, 'ACTIVO') RETURNING *

-- Editar alumno
-- :name update-student-by-id :! :1
UPDATE students 
SET name = :name,
  surname = :surname,
  inscription = :inscription
--~ (when (contains? params :phone) ",phone = :phone")
--~ (when (contains? params :mail) ",mail = :mail")
WHERE id = :id

-- Establece token para el alumno
-- :name student-set-activation-code :! :1
UPDATE students
   SET code = :code,
       token = NULL,
       activation_date = NULL
 WHERE id = :id


-- Cambia el estado de activación
-- :name toggle-state-student :! :1
UPDATE students
   SET state = CASE state
               WHEN 'ACTIVO' THEN 'DESACTIVADO'
               ELSE               'ACTIVO'
               END,
       token = NULL,
       code = NULL,
       activation_date = NULL
 WHERE id = :id


-- Actualiza las notas/observaciones del alumno
-- :name update-student-notes :! :1
UPDATE students
SET notes = :notes
WHERE id = :id

