(ns kaeme.students.core
  (:use kaeme.macros)
  (:require
    [clojure.java.io :as io]
    [io.pedestal.log :as log]
    [hugsql.core :as hugsql]
    [clojure.java.jdbc :as jdbc]
    [clojure.java.jdbc :refer [with-db-transaction]]
    [clojure.string :as str]
    [kaeme.notifications.db :refer [add-notification]]
    [kaeme.students.db :as db]))

(defn all-students [db _ args _]
  (let [args (cond-> args
               (:order args) (update :order name))]
    (db/all-students db args)))

(defn get-student [db {user-info :user-info} args _]
  (when-let [id (or (and (contains? user-info :teacher) (:id args))
                    (:student user-info))]
    (db/student-by-id db {:id id})))

(defn delete-student [db ctx {id :id} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                student (db/student-by-id tx{:id id})]
      (db/delete-student-by-id tx{:id id})
      (add-notification tx teacher (format "Se elimina alumno %s %s" (:name student) (:surname student)))
      student)))

(defn create-student [db ctx args _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                data    (merge {:phone nil :mail nil} (:data args))
                student (first (db/insert-student tx data))]
      (add-notification tx teacher (:id student) "Registro de nuevo alumno")
      student)))

(defn update-student [db ctx args _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                db-args (merge (:data args) {:id (:id args)})]
      (add-notification tx teacher (:id db-args) "Modificación del alumno")
      (db/update-student-by-id tx db-args)
      (db/student-by-id tx args))))

(defn- new-activation-code []
  (->> #(rand-nth "123456789ABCDEFGHJKLMNPQRSTUVWXYZ")
       repeatedly
       (take 6)
       (apply str)))

(defn toggle-state-student [db ctx {id :id} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                n       (db/toggle-state-student tx {:id id})
                student (db/student-by-id tx {:id id})]
      (add-notification tx teacher id (format "Cambio de estado %s" (:state student)))
      n)))

(defn new-activation-code-student [db ctx {id :id} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                activation-code (new-activation-code)]
      (db/student-set-activation-code tx {:id id :code activation-code})
      (add-notification tx teacher id "Nuevo código de activación")
      activation-code)))

(defn get-student-notes [_ {user-info :user-info} _ {notes :notes}]
  (when (contains? user-info :teacher)
    notes))

(defn update-student-notes [db ctx args _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                student (db/student-by-id tx args)]
      (add-notification tx teacher (:id args) "Se actualizan observaciones")
      (db/update-student-notes tx args))))

(def resolver-map
  {:Student/notes get-student-notes
   :Query/Students all-students
   :Query/Student get-student
   :Mutation/DeleteStudent delete-student
   :Mutation/CreateStudent create-student
   :Mutation/UpdateStudent update-student
   :Mutation/ToggleStateStudent toggle-state-student
   :Mutation/NewActivationCodeStudent new-activation-code-student
   :Mutation/UpdateStudentNotes update-student-notes})

(def schema (slurp (io/resource "kaeme/students/students.graphql")))
