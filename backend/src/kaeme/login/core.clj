(ns kaeme.login.core
  (:require
    [clojure.java.io :as io]
    [io.pedestal.http.body-params :refer [body-params]]
    [io.pedestal.log :as log]
    [ring.util.response :as response]
    [hugsql.core :as hugsql]
    [clojure.java.jdbc :as jdbc]
    [clojure.string :as str]
    [kaeme.login.db :as db]
    [cheshire.core :as ch]
    [crypto.password.scrypt :as password]
    [kaeme.db :refer [database]]))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(defn- json-response [response]
  (-> response
      ch/generate-string
      response/response
      (response/content-type "application/json; charset=utf-8")))

(defn- student-activate [request]
  (let [code  (get-in request [:form-params :code])
        token (uuid)]
    (if (nil? code)
      (response/bad-request "You must send activation code")
      (if (= 1 (db/activate-code database {:token token :code code}))
        (json-response {:token token})
        (json-response {:error "Invalid code"})))))

(defn- admin-login [request]
  (let [mail     (get-in request [:form-params :mail])
        password (get-in request [:form-params :password])
        user     (and mail (db/get-user-by-mail database {:mail mail}))
        token    (uuid)]
    (cond
      (or (nil? mail) (nil? password)) (response/bad-request "You must send mail and password")
      (nil? user) (json-response {:error "Correo no válido"})
      (not (password/check password (:password user))) (json-response {:error "Contraseña no válida"})
      :else (do (db/update-user-token database {:token token :user_id (:id user)})
                (json-response {:token token})))))

(def login-routes
  #{ ["/student-activate" :post [(body-params) student-activate] :route-name ::student-activate]
     ["/admin-login"      :post [(body-params) admin-login]      :route-name ::admin-login]})




