(ns kaeme.login.interceptors
  (:require [mount.core :refer [defstate]]
            [ring.util.response :as response]
            [clojure.java.io :as io]
            [io.pedestal.log :as log]
            [io.pedestal.http :as http]
            [io.pedestal.interceptor :refer [interceptor]]
            [com.walmartlabs.lacinia.pedestal.interceptors :as pi]
            [com.walmartlabs.lacinia.pedestal2 :as p2]
            [com.walmartlabs.lacinia.pedestal :refer [inject]]
            [kaeme.login.db :as db]
            [kaeme.db :refer [database]]
            [hugsql.core :as hugsql]))


(defn- extract-user-info
  [request]
  (let [authorization (get-in request [:headers "authorization"])
        token         (last (re-find #"^Bearer (.*)$" (str authorization)))
        user-info     (if (some? token) (db/search-token database {:token token}))]
    (if-let [{:keys [type id]} user-info]
      (hash-map (keyword type) id))))


(def ^:private user-info-interceptor
  (interceptor
   {:name ::user-info
    :enter (fn [context]
              (let [{:keys [request]} context
                    user-info (extract-user-info request)]
                (if (nil? user-info)
                  (throw (Exception. "AuthError")))
                (assoc-in context [:request :lacinia-app-context :user-info] user-info)))}))

(defn interceptors
  [schema]
  (-> (p2/default-interceptors schema nil)
      (inject user-info-interceptor :after ::p2/inject-app-context)))