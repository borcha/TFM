(ns kaeme.login.db
  (:require [hugsql.core :as hugsql]
            [io.pedestal.log :as log]
            [cheshire.core :as ch]))

(hugsql/def-db-fns "kaeme/login/login.sql")
