-- Obtener listado de alumnos
-- :name search-token :? :1
SELECT 'student' type, id
FROM students
WHERE token = :token
UNION
SELECT 'teacher', id
FROM users
WHERE token = :token

-- Activar código del alumno
-- :name activate-code :! :n
UPDATE students
   SET code = NULL,
       token = :token,
       activation_date = datetime()
 WHERE code = :code


-- Buscar usuario por mail
-- :name get-user-by-mail :? :1
SELECT * FROM users WHERE mail = :mail

-- Actualizar token de un usuario
-- :name update-user-token :! :n
UPDATE users
   SET token = :token
 WHERE id = :user_id
 