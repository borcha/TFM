(ns kaeme.schema
  "Contains custom resolvers and a function to provide the full schema."
  (:require
    [clojure.java.io :as io]
    [clojure.string :as str]
    [mount.core :refer [defstate]]
    [com.walmartlabs.lacinia.util :as util]
    [com.walmartlabs.lacinia.resolve :refer [resolve-as]]
    [com.walmartlabs.lacinia.schema :as schema]
    [com.walmartlabs.lacinia.parser.schema :refer [parse-schema]]
    [io.pedestal.log :as log]
    [kaeme.students.core :as students]
    [kaeme.notifications.core :as notifications]
    [kaeme.users.core :as users]
    [kaeme.cards.core :as cards]
    [kaeme.syllabus.core :as syllabus]
    [kaeme.progress.core :as progress]
    [kaeme.db :refer [database]]))

(import
  java.text.SimpleDateFormat
  java.util.Date)

(def date-formatter
  "Used by custom scalar :Date."
  (SimpleDateFormat. "yyyy-MM-dd"))

(def scalars
  { :Date { :parse identity
            :serialize identity}})


(defn update-map [m f]
  (reduce-kv
    (fn [m k v]
      (assoc m k (f v))) {} m))

(defn resolver-map [database]
  (let [resolvers [students/resolver-map
                   users/resolver-map
                   notifications/resolver-map
                   syllabus/resolver-map
                   cards/resolver-map
                   progress/resolver-map]
        merged-map (apply merge resolvers)]
    (update-map merged-map #(partial % database))))

(def base-schema (slurp (io/resource "kaeme/base.graphql")))

(defn load-schema [database]
  (let [schemas [base-schema
                 students/schema
                 users/schema
                 notifications/schema
                 syllabus/schema
                 cards/schema
                 progress/schema]]
    (-> (apply str schemas)
        parse-schema
        (util/inject-scalar-transformers scalars)
        (util/inject-resolvers (resolver-map database))
        schema/compile)))

(defstate schema :start (load-schema database))
