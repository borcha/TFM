(ns kaeme.server
  (:require [mount.core :refer [defstate]]
            [ring.util.response :as response]
            [clojure.java.io :as io]
            [io.pedestal.log :as log]
            [com.walmartlabs.lacinia.pedestal2 :as lp]
            [io.pedestal.http :as http]
            [kaeme.login.interceptors :refer [interceptors]]
            [kaeme.login.core :refer [login-routes]]
            [kaeme.schema :refer [schema]]))

(defn render-index [base]
  (fn [request]
    (-> (str "public/" base "/index.html")
        response/resource-response
        (response/content-type "text/html"))))

(defn render-admin [request]
  (-> "public/admin/index.html"
      response/resource-response
      (response/content-type "text/html")))

(defn- create-routes [interceptors]
  (-> #{["/"              :get (render-index "")        :route-name ::render-index]
        ["/student"       :get (render-index "student") :route-name ::render-student-index]
        ["/student/"      :get (render-index "student") :route-name ::render-student-index2]
        ["/student/*path" :get (render-index "student") :route-name ::render-student]
        ["/admin"         :get (render-index "admin")   :route-name ::render-admin-index]
        ["/admin/"        :get (render-index "admin")   :route-name ::render-admin-index2]
        ["/admin/*path"   :get (render-index "admin")   :route-name ::render-admin]
        ["/api"           :post interceptors            :route-name ::graphql-api]
        ["/ide"           :get (lp/graphiql-ide-handler nil) :route-name ::graphiql-ide]}
      (into (lp/graphiql-asset-routes "/assets/graphiql"))
      (into login-routes)))

(defn- create-server
  [compiled-schema]
  (let [interceptors (interceptors compiled-schema)]
    (-> {:env :dev
         ::http/allowed-origins (constantly true)
         ::http/resource-path "/public"
         ::http/secure-headers nil
         ::http/routes (create-routes interceptors)
         ::http/port 8888
         ::http/host "0.0.0.0"
         ::http/type :jetty
         ::http/join? false}
        http/create-server
        http/start)))

(defstate server
  :start (-> schema
             create-server)

  :stop (http/stop server))
