-- Obtener listado de usuarios
-- :name all-users :? :*
select * from users

-- Obtener usuario
-- :name user-by-id :? :1
select * from users where id = :id

-- Eliminar usuario
-- :name delete-user-by-id :! :n
delete from users where id = :id

-- Insertar usuario
-- :name insert-user :<!
INSERT INTO users (name, mail, password)
VALUES(:name, :mail, :password) RETURNING *

-- Editar usuario
-- :name update-user-by-id :! :1
UPDATE users 
SET name = :name,
    mail = :mail
--~ (when (contains? params :password) ", password = :password")   
WHERE id = :id
