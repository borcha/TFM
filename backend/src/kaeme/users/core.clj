(ns kaeme.users.core
  (:use kaeme.macros)
  (:require
    [clojure.java.io :as io]
    [io.pedestal.log :as log]
    [hugsql.core :as hugsql]
    [clojure.java.jdbc :as jdbc]
    [clojure.java.jdbc :refer [with-db-transaction]]
    [clojure.string :as str]
    [crypto.password.scrypt :as password]
    [kaeme.notifications.db :refer [add-notification]]
    [kaeme.users.db :as db]))

(defn all-users [db _ args _]
  (db/all-users db))

(defn get-user [db ctx {id :id} _]
  (db/user-by-id db {:id id}))

(defn delete-user [db ctx {id :id} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                user    (db/user-by-id tx {:id id})]
      (db/delete-user-by-id tx {:id id})
      (add-notification tx teacher (format "Se elimina usuario #%d-%s" (:id user) (:name user)))
      user)))

(defn create-user [db ctx {data :data} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                data    (cond-> data
                          (:password data) (update :password password/encrypt))
                user    (first (db/insert-user tx data))]
      (add-notification tx teacher (format "Se crea usuario #%d-%s" (:id user) (:name user)))
      user)))

(defn update-user [db ctx {id :id data :data} _]
  (with-db-transaction [tx db]
    (when-let* [teacher (get-in ctx [:user-info :teacher])
                db-args (cond-> (assoc data :id id)
                          (:password data) (update :password password/encrypt))
                _       (db/update-user-by-id tx db-args)
                user    (db/user-by-id tx db-args)]
      (add-notification tx teacher (format "Se modifica usuario #%d-%s" (:id user) (:name user)))
      user)))

(def resolver-map
  {:Query/users all-users
   :Query/user get-user
   :Mutation/DeleteUser delete-user
   :Mutation/CreateUser create-user
   :Mutation/UpdateUser update-user})

(def schema (slurp (io/resource "kaeme/users/users.graphql")))

