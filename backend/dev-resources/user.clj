(ns user
  (:require
    [com.walmartlabs.lacinia :as lacinia]
    [clojure.java.browse :refer [browse-url]]
    [kaeme.db :refer [migrations]]
    [kaeme.schema :refer [schema]]
    [clojure.tools.namespace.repl :as tn]
    [migratus.core :as migratus]
    [mount.core :as mount]))

(defn q
  [query-string]
  (-> schema
      (lacinia/execute query-string nil nil)))

(defn start []
  (mount/start))

(defn stop []
  (mount/stop))

(defn refresh []
  (stop)
  (tn/refresh))

(defn refresh-all []
  (stop)
  (tn/refresh-all)
  (start))

(defn go
  "starts all states defined by defstate"
  []
  (start)
  :ready)

(defn reset
  "stops all states defined by defstate, reloads modified source files, and restarts the states"
  []
  (stop)
  (tn/refresh :after 'user/go))

(comment

  (require '[kaeme.db :refer [database] :rename {database db}])
  (migratus/create migrations "notifications")
  (migratus/up migrations 20111206154000)
  (migratus/down migrations 20210530022357)

  (none))
