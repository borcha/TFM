FROM clojure:openjdk-11-lein-slim-buster as builder

RUN apt update \
  && apt -y install curl gnupg \
  && curl -sL https://deb.nodesource.com/setup_14.x  | bash - \
  && apt-get -y install nodejs \
  && npm install

COPY . /src

WORKDIR /src/

RUN ./build.sh

FROM adoptopenjdk/openjdk11:jre-11.0.11_9-alpine
COPY --from=builder /src/backend/target/uberjar/kaeme.jar /app/kaeme.jar
RUN mkdir /app/db
EXPOSE 8888
ENTRYPOINT [ "java", "-jar", "/app/kaeme.jar" ]

