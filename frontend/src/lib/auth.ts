// Guardar autenticación en el localStorage
window.AUTH_TOKEN = '__AUTH_TOKEN__'

export function teacherMode() {
  window.AUTH_TOKEN = '__TEACHER_AUTH_TOKEN__'
}

export function getAuthToken() {
  let authToken = localStorage.getItem(window.AUTH_TOKEN);
  return authToken;
}

export function saveAuthToken(authToken:string) {
  localStorage.setItem(window.AUTH_TOKEN, authToken);
}

export function removeAuthToken() {
  localStorage.removeItem(window.AUTH_TOKEN);
}

export function reActivate() {
  removeAuthToken();
  window.location.reload();  
}
