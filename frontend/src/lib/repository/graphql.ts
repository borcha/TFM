import { GraphQLClient } from 'graphql-request';
import * as Dom from 'graphql-request/dist/types.dom';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Date: any;
};

/** Estadística desde el último mes hasta el dia actual */
export type Activity = {
  __typename?: 'Activity';
  sessions: Array<Scalars['Int']>;
};

/** Tarjetas de repaso */
export type Card = {
  __typename?: 'Card';
  id: Scalars['Int'];
  title: Scalars['String'];
  question: Scalars['String'];
  answer: Scalars['String'];
  syllabus_id: Scalars['Int'];
};

export type CardData = {
  title: Scalars['String'];
  question: Scalars['String'];
  answer: Scalars['String'];
  syllabus_id: Scalars['Int'];
};

/** Representa un capítulo del temario */
export type Chapter = {
  __typename?: 'Chapter';
  content?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  last?: Maybe<Scalars['Boolean']>;
  level: Scalars['Int'];
  parent?: Maybe<Scalars['Int']>;
  pos: Scalars['Int'];
  progress?: Maybe<Progress>;
  title: Scalars['String'];
  totalCards?: Maybe<Scalars['Int']>;
};


/** Representa un capítulo del temario */
export type ChapterProgressArgs = {
  id?: Maybe<Scalars['Int']>;
};

export type ChapterData = {
  title: Scalars['String'];
  content?: Maybe<Scalars['String']>;
  parent?: Maybe<Scalars['Int']>;
};


export type Mutation = {
  __typename?: 'Mutation';
  CreateCard?: Maybe<Card>;
  CreateChapter?: Maybe<Chapter>;
  CreateSession?: Maybe<Progress>;
  CreateStudent?: Maybe<Student>;
  CreateUser?: Maybe<User>;
  DeleteCard?: Maybe<Card>;
  DeleteChapter?: Maybe<Chapter>;
  DeleteProgress?: Maybe<Scalars['Boolean']>;
  DeleteStudent?: Maybe<Student>;
  DeleteUser?: Maybe<User>;
  NewActivationCodeStudent: Scalars['String'];
  ReorderChapter?: Maybe<Array<Maybe<Chapter>>>;
  ToggleStateStudent?: Maybe<Scalars['Int']>;
  UpdateCard?: Maybe<Card>;
  UpdateChapter?: Maybe<Chapter>;
  UpdateStudent?: Maybe<Student>;
  UpdateStudentNotes?: Maybe<Scalars['Int']>;
  UpdateUser?: Maybe<User>;
  modifica?: Maybe<Scalars['Int']>;
};


export type MutationCreateCardArgs = {
  data: CardData;
};


export type MutationCreateChapterArgs = {
  data: ChapterData;
};


export type MutationCreateSessionArgs = {
  chapter: Scalars['Int'];
  data: SessionData;
};


export type MutationCreateStudentArgs = {
  data: StudentData;
};


export type MutationCreateUserArgs = {
  data: UserData;
};


export type MutationDeleteCardArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteChapterArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteProgressArgs = {
  chapter: Scalars['Int'];
};


export type MutationDeleteStudentArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteUserArgs = {
  id: Scalars['Int'];
};


export type MutationNewActivationCodeStudentArgs = {
  id: Scalars['Int'];
};


export type MutationReorderChapterArgs = {
  id: Scalars['Int'];
  operation: SyllabusReorder;
};


export type MutationToggleStateStudentArgs = {
  id: Scalars['Int'];
};


export type MutationUpdateCardArgs = {
  id: Scalars['Int'];
  data: CardData;
};


export type MutationUpdateChapterArgs = {
  id: Scalars['Int'];
  data: ChapterData;
};


export type MutationUpdateStudentArgs = {
  id: Scalars['Int'];
  data: StudentData;
};


export type MutationUpdateStudentNotesArgs = {
  id: Scalars['Int'];
  notes?: Maybe<Scalars['String']>;
};


export type MutationUpdateUserArgs = {
  id: Scalars['Int'];
  data: UserData;
};

export type Notification = {
  __typename?: 'Notification';
  date: Scalars['Date'];
  message: Scalars['String'];
  user_name?: Maybe<Scalars['String']>;
  student_name?: Maybe<Scalars['String']>;
};

/** Estado del progreso de un capítulo */
export type Progress = {
  __typename?: 'Progress';
  pending: Scalars['Int'];
  inprogress: Scalars['Int'];
  learned: Scalars['Int'];
  date: Scalars['Date'];
};

export type Query = {
  __typename?: 'Query';
  /** Obtener una tarjeta en particular */
  Card?: Maybe<Card>;
  /** Lista de tarjetas de repaso */
  Cards?: Maybe<Array<Maybe<Card>>>;
  /** Obtener un tema en particular */
  Chapter?: Maybe<Chapter>;
  GetSession?: Maybe<Array<Card>>;
  /** Mostrar notificaciones */
  Notifications?: Maybe<Array<Maybe<Notification>>>;
  /** Obtener un estudiante */
  Student?: Maybe<Student>;
  /** Listar todos los estudiantes */
  Students?: Maybe<Array<Maybe<Student>>>;
  /** Obtener temario ordenado */
  Syllabus?: Maybe<Array<Maybe<Chapter>>>;
  saludar?: Maybe<Scalars['Int']>;
  /** Obtener un usuario */
  user?: Maybe<User>;
  /** Listar los usuarios administradores */
  users?: Maybe<Array<Maybe<User>>>;
};


export type QueryCardArgs = {
  id?: Maybe<Scalars['Int']>;
};


export type QueryCardsArgs = {
  syllabus?: Maybe<Scalars['Int']>;
};


export type QueryChapterArgs = {
  id?: Maybe<Scalars['Int']>;
  pos?: Maybe<Scalars['Int']>;
};


export type QueryGetSessionArgs = {
  chapter: Scalars['Int'];
};


export type QueryNotificationsArgs = {
  id?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
};


export type QueryStudentArgs = {
  id?: Maybe<Scalars['Int']>;
};


export type QueryStudentsArgs = {
  order?: Maybe<StudentsOrderColumns>;
};


export type QueryUserArgs = {
  id: Scalars['Int'];
};

export type SessionData = {
  date?: Maybe<Scalars['Date']>;
  duration: Scalars['Int'];
  hits: Array<Scalars['Int']>;
  faults: Array<Scalars['Int']>;
};

export type Student = {
  __typename?: 'Student';
  activation_date?: Maybe<Scalars['Date']>;
  activity?: Maybe<Activity>;
  id: Scalars['Int'];
  inscription: Scalars['Date'];
  lastChapter?: Maybe<Chapter>;
  mail?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  notes?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
  progress?: Maybe<Progress>;
  state: StudentStates;
  surname: Scalars['String'];
};

export type StudentData = {
  name: Scalars['String'];
  surname: Scalars['String'];
  inscription: Scalars['Date'];
  mail?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
};

export enum StudentStates {
  Activo = 'ACTIVO',
  Desactivado = 'DESACTIVADO'
}

export enum StudentsOrderColumns {
  Name = 'name',
  Surname = 'surname',
  Inscription = 'inscription'
}

export enum SyllabusReorder {
  Up = 'UP',
  Down = 'DOWN',
  Left = 'LEFT',
  Right = 'RIGHT'
}

export type User = {
  __typename?: 'User';
  id: Scalars['Int'];
  name: Scalars['String'];
  mail?: Maybe<Scalars['String']>;
};

export type UserData = {
  name: Scalars['String'];
  mail: Scalars['String'];
  password?: Maybe<Scalars['String']>;
};

export type AllCardsQueryVariables = Exact<{
  syllabus?: Maybe<Scalars['Int']>;
}>;


export type AllCardsQuery = (
  { __typename?: 'Query' }
  & { Cards?: Maybe<Array<Maybe<(
    { __typename?: 'Card' }
    & Pick<Card, 'id' | 'title'>
  )>>>, Syllabus?: Maybe<Array<Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'level' | 'totalCards'>
  )>>> }
);

export type GetCardQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetCardQuery = (
  { __typename?: 'Query' }
  & { card?: Maybe<(
    { __typename?: 'Card' }
    & Pick<Card, 'title' | 'question' | 'answer' | 'syllabus_id'>
  )>, syllabus?: Maybe<Array<Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'level'>
  )>>> }
);

export type DeleteCardMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteCardMutation = (
  { __typename?: 'Mutation' }
  & { card?: Maybe<(
    { __typename?: 'Card' }
    & Pick<Card, 'id'>
  )> }
);

export type CreateCardMutationVariables = Exact<{
  card: CardData;
}>;


export type CreateCardMutation = (
  { __typename?: 'Mutation' }
  & { card?: Maybe<(
    { __typename?: 'Card' }
    & Pick<Card, 'id'>
  )> }
);

export type UpdateCardMutationVariables = Exact<{
  id: Scalars['Int'];
  card: CardData;
}>;


export type UpdateCardMutation = (
  { __typename?: 'Mutation' }
  & { card?: Maybe<(
    { __typename?: 'Card' }
    & Pick<Card, 'id'>
  )> }
);

export type SessionCardsQueryVariables = Exact<{
  syllabus: Scalars['Int'];
}>;


export type SessionCardsQuery = (
  { __typename?: 'Query' }
  & { chapter?: Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title'>
  )>, cards?: Maybe<Array<(
    { __typename?: 'Card' }
    & Pick<Card, 'id' | 'question' | 'answer'>
  )>> }
);

export type CreateSessionMutationVariables = Exact<{
  chapter: Scalars['Int'];
  data: SessionData;
}>;


export type CreateSessionMutation = (
  { __typename?: 'Mutation' }
  & { progress?: Maybe<(
    { __typename?: 'Progress' }
    & ProgressFieldsFragment
  )> }
);

export type AllStudentsQueryVariables = Exact<{ [key: string]: never; }>;


export type AllStudentsQuery = (
  { __typename?: 'Query' }
  & { students?: Maybe<Array<Maybe<(
    { __typename?: 'Student' }
    & Pick<Student, 'id' | 'name' | 'surname' | 'inscription' | 'state'>
  )>>> }
);

export type GetStudentQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetStudentQuery = (
  { __typename?: 'Query' }
  & { user?: Maybe<(
    { __typename?: 'Student' }
    & Pick<Student, 'name' | 'surname' | 'inscription' | 'phone' | 'mail' | 'state' | 'activation_date'>
  )> }
);

export type ProgressFieldsFragment = (
  { __typename?: 'Progress' }
  & Pick<Progress, 'inprogress' | 'learned' | 'pending'>
);

export type HomeStudentQueryVariables = Exact<{ [key: string]: never; }>;


export type HomeStudentQuery = (
  { __typename?: 'Query' }
  & { user?: Maybe<(
    { __typename?: 'Student' }
    & Pick<Student, 'name' | 'surname'>
    & { progress?: Maybe<(
      { __typename?: 'Progress' }
      & ProgressFieldsFragment
    )>, lastChapter?: Maybe<(
      { __typename?: 'Chapter' }
      & Pick<Chapter, 'id' | 'title' | 'last' | 'totalCards' | 'pos'>
      & { progress?: Maybe<(
        { __typename?: 'Progress' }
        & ProgressFieldsFragment
      )> }
    )>, activity?: Maybe<(
      { __typename?: 'Activity' }
      & Pick<Activity, 'sessions'>
    )> }
  )> }
);

export type DeleteStudentMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteStudentMutation = (
  { __typename?: 'Mutation' }
  & { student?: Maybe<(
    { __typename?: 'Student' }
    & Pick<Student, 'id'>
  )> }
);

export type CreateStudentMutationVariables = Exact<{
  user: StudentData;
}>;


export type CreateStudentMutation = (
  { __typename?: 'Mutation' }
  & { student?: Maybe<(
    { __typename?: 'Student' }
    & Pick<Student, 'id'>
  )> }
);

export type UpdateStudentMutationVariables = Exact<{
  id: Scalars['Int'];
  user: StudentData;
}>;


export type UpdateStudentMutation = (
  { __typename?: 'Mutation' }
  & { student?: Maybe<(
    { __typename?: 'Student' }
    & Pick<Student, 'id'>
  )> }
);

export type NewActivationCodeMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type NewActivationCodeMutation = (
  { __typename?: 'Mutation' }
  & { activationCode: Mutation['NewActivationCodeStudent'] }
);

export type ToggleStateStudentMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type ToggleStateStudentMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'ToggleStateStudent'>
);

export type GetSyllabusQueryVariables = Exact<{ [key: string]: never; }>;


export type GetSyllabusQuery = (
  { __typename?: 'Query' }
  & { syllabus?: Maybe<Array<Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'level' | 'pos'>
  )>>> }
);

export type GetStudentSyllabusQueryVariables = Exact<{ [key: string]: never; }>;


export type GetStudentSyllabusQuery = (
  { __typename?: 'Query' }
  & { syllabus?: Maybe<Array<Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'level' | 'pos'>
    & { progress?: Maybe<(
      { __typename?: 'Progress' }
      & ProgressFieldsFragment
    )> }
  )>>> }
);

export type GetChapterQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetChapterQuery = (
  { __typename?: 'Query' }
  & { chapter?: Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'content' | 'parent' | 'totalCards'>
  )>, syllabus?: Maybe<Array<Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'level'>
  )>>> }
);

export type GetChapterByPosQueryVariables = Exact<{
  pos: Scalars['Int'];
}>;


export type GetChapterByPosQuery = (
  { __typename?: 'Query' }
  & { chapter?: Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'content' | 'last' | 'totalCards'>
    & { progress?: Maybe<(
      { __typename?: 'Progress' }
      & ProgressFieldsFragment
    )> }
  )> }
);

export type DeleteChapterMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteChapterMutation = (
  { __typename?: 'Mutation' }
  & { chapter?: Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id'>
  )> }
);

export type CreateChapterMutationVariables = Exact<{
  chapter: ChapterData;
}>;


export type CreateChapterMutation = (
  { __typename?: 'Mutation' }
  & { chapter?: Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id'>
  )> }
);

export type UpdateChapterMutationVariables = Exact<{
  id: Scalars['Int'];
  chapter: ChapterData;
}>;


export type UpdateChapterMutation = (
  { __typename?: 'Mutation' }
  & { chapter?: Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id'>
  )> }
);

export type DeleteProgressMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteProgressMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'DeleteProgress'>
);

export type ReorderChapterMutationVariables = Exact<{
  id: Scalars['Int'];
  operation: SyllabusReorder;
}>;


export type ReorderChapterMutation = (
  { __typename?: 'Mutation' }
  & { syllabus?: Maybe<Array<Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'id' | 'title' | 'level' | 'pos'>
  )>>> }
);

export type ShowNotificationsQueryVariables = Exact<{ [key: string]: never; }>;


export type ShowNotificationsQuery = (
  { __typename?: 'Query' }
  & { notifications?: Maybe<Array<Maybe<(
    { __typename?: 'Notification' }
    & Pick<Notification, 'date' | 'message' | 'user_name' | 'student_name'>
  )>>> }
);

export type ShowStudentQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type ShowStudentQuery = (
  { __typename?: 'Query' }
  & { user?: Maybe<(
    { __typename?: 'Student' }
    & Pick<Student, 'name' | 'surname' | 'mail' | 'phone' | 'notes'>
    & { progress?: Maybe<(
      { __typename?: 'Progress' }
      & ProgressFieldsFragment
    )>, activity?: Maybe<(
      { __typename?: 'Activity' }
      & Pick<Activity, 'sessions'>
    )> }
  )>, chapters?: Maybe<Array<Maybe<(
    { __typename?: 'Chapter' }
    & Pick<Chapter, 'title'>
    & { progress?: Maybe<(
      { __typename?: 'Progress' }
      & Pick<Progress, 'date'>
      & ProgressFieldsFragment
    )> }
  )>>>, notifications?: Maybe<Array<Maybe<(
    { __typename?: 'Notification' }
    & Pick<Notification, 'date' | 'message' | 'user_name' | 'student_name'>
  )>>> }
);

export type SaveNotesMutationVariables = Exact<{
  id: Scalars['Int'];
  notes: Scalars['String'];
}>;


export type SaveNotesMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'UpdateStudentNotes'>
);

export type AllUsersQueryVariables = Exact<{ [key: string]: never; }>;


export type AllUsersQuery = (
  { __typename?: 'Query' }
  & { users?: Maybe<Array<Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id' | 'name' | 'mail'>
  )>>> }
);

export type GetUserQueryVariables = Exact<{
  id: Scalars['Int'];
}>;


export type GetUserQuery = (
  { __typename?: 'Query' }
  & { user?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'name' | 'mail'>
  )> }
);

export type DeleteUserMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteUserMutation = (
  { __typename?: 'Mutation' }
  & { user?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id'>
  )> }
);

export type CreateUserMutationVariables = Exact<{
  user: UserData;
}>;


export type CreateUserMutation = (
  { __typename?: 'Mutation' }
  & { user?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id'>
  )> }
);

export type UpdateUserMutationVariables = Exact<{
  id: Scalars['Int'];
  user: UserData;
}>;


export type UpdateUserMutation = (
  { __typename?: 'Mutation' }
  & { user?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'id'>
  )> }
);

export const ProgressFieldsFragmentDoc = gql`
    fragment progressFields on Progress {
  inprogress
  learned
  pending
}
    `;
export const AllCardsDocument = gql`
    query allCards($syllabus: Int) {
  Cards(syllabus: $syllabus) {
    id
    title
  }
  Syllabus {
    id
    title
    level
    totalCards
  }
}
    `;
export const GetCardDocument = gql`
    query getCard($id: Int!) {
  card: Card(id: $id) {
    title
    question
    answer
    syllabus_id
  }
  syllabus: Syllabus {
    id
    title
    level
  }
}
    `;
export const DeleteCardDocument = gql`
    mutation deleteCard($id: Int!) {
  card: DeleteCard(id: $id) {
    id
  }
}
    `;
export const CreateCardDocument = gql`
    mutation createCard($card: CardData!) {
  card: CreateCard(data: $card) {
    id
  }
}
    `;
export const UpdateCardDocument = gql`
    mutation updateCard($id: Int!, $card: CardData!) {
  card: UpdateCard(id: $id, data: $card) {
    id
  }
}
    `;
export const SessionCardsDocument = gql`
    query sessionCards($syllabus: Int!) {
  chapter: Chapter(id: $syllabus) {
    id
    title
  }
  cards: GetSession(chapter: $syllabus) {
    id
    question
    answer
  }
}
    `;
export const CreateSessionDocument = gql`
    mutation createSession($chapter: Int!, $data: SessionData!) {
  progress: CreateSession(chapter: $chapter, data: $data) {
    ...progressFields
  }
}
    ${ProgressFieldsFragmentDoc}`;
export const AllStudentsDocument = gql`
    query allStudents {
  students: Students {
    id
    name
    surname
    inscription
    state
  }
}
    `;
export const GetStudentDocument = gql`
    query getStudent($id: Int!) {
  user: Student(id: $id) {
    name
    surname
    inscription
    phone
    mail
    state
    activation_date
  }
}
    `;
export const HomeStudentDocument = gql`
    query homeStudent {
  user: Student {
    name
    surname
    progress {
      ...progressFields
    }
    lastChapter {
      id
      title
      last
      totalCards
      pos
      progress {
        ...progressFields
      }
    }
    activity {
      sessions
    }
  }
}
    ${ProgressFieldsFragmentDoc}`;
export const DeleteStudentDocument = gql`
    mutation deleteStudent($id: Int!) {
  student: DeleteStudent(id: $id) {
    id
  }
}
    `;
export const CreateStudentDocument = gql`
    mutation createStudent($user: StudentData!) {
  student: CreateStudent(data: $user) {
    id
  }
}
    `;
export const UpdateStudentDocument = gql`
    mutation updateStudent($id: Int!, $user: StudentData!) {
  student: UpdateStudent(id: $id, data: $user) {
    id
  }
}
    `;
export const NewActivationCodeDocument = gql`
    mutation newActivationCode($id: Int!) {
  activationCode: NewActivationCodeStudent(id: $id)
}
    `;
export const ToggleStateStudentDocument = gql`
    mutation toggleStateStudent($id: Int!) {
  ToggleStateStudent(id: $id)
}
    `;
export const GetSyllabusDocument = gql`
    query getSyllabus {
  syllabus: Syllabus {
    id
    title
    level
    pos
  }
}
    `;
export const GetStudentSyllabusDocument = gql`
    query getStudentSyllabus {
  syllabus: Syllabus {
    id
    title
    level
    pos
    progress {
      ...progressFields
    }
  }
}
    ${ProgressFieldsFragmentDoc}`;
export const GetChapterDocument = gql`
    query getChapter($id: Int!) {
  chapter: Chapter(id: $id) {
    id
    title
    content
    parent
    totalCards
  }
  syllabus: Syllabus {
    id
    title
    level
  }
}
    `;
export const GetChapterByPosDocument = gql`
    query getChapterByPos($pos: Int!) {
  chapter: Chapter(pos: $pos) {
    id
    title
    content
    last
    totalCards
    progress {
      ...progressFields
    }
  }
}
    ${ProgressFieldsFragmentDoc}`;
export const DeleteChapterDocument = gql`
    mutation deleteChapter($id: Int!) {
  chapter: DeleteChapter(id: $id) {
    id
  }
}
    `;
export const CreateChapterDocument = gql`
    mutation createChapter($chapter: ChapterData!) {
  chapter: CreateChapter(data: $chapter) {
    id
  }
}
    `;
export const UpdateChapterDocument = gql`
    mutation updateChapter($id: Int!, $chapter: ChapterData!) {
  chapter: UpdateChapter(id: $id, data: $chapter) {
    id
  }
}
    `;
export const DeleteProgressDocument = gql`
    mutation deleteProgress($id: Int!) {
  DeleteProgress(chapter: $id)
}
    `;
export const ReorderChapterDocument = gql`
    mutation reorderChapter($id: Int!, $operation: SyllabusReorder!) {
  syllabus: ReorderChapter(id: $id, operation: $operation) {
    id
    title
    level
    pos
  }
}
    `;
export const ShowNotificationsDocument = gql`
    query showNotifications {
  notifications: Notifications {
    date
    message
    user_name
    student_name
  }
}
    `;
export const ShowStudentDocument = gql`
    query showStudent($id: Int!) {
  user: Student(id: $id) {
    name
    surname
    mail
    phone
    notes
    progress {
      ...progressFields
    }
    activity {
      sessions
    }
  }
  chapters: Syllabus {
    title
    progress(id: $id) {
      date
      ...progressFields
    }
  }
  notifications: Notifications(id: $id, limit: 20) {
    date
    message
    user_name
    student_name
  }
}
    ${ProgressFieldsFragmentDoc}`;
export const SaveNotesDocument = gql`
    mutation saveNotes($id: Int!, $notes: String!) {
  UpdateStudentNotes(id: $id, notes: $notes)
}
    `;
export const AllUsersDocument = gql`
    query allUsers {
  users {
    id
    name
    mail
  }
}
    `;
export const GetUserDocument = gql`
    query getUser($id: Int!) {
  user(id: $id) {
    name
    mail
  }
}
    `;
export const DeleteUserDocument = gql`
    mutation deleteUser($id: Int!) {
  user: DeleteUser(id: $id) {
    id
  }
}
    `;
export const CreateUserDocument = gql`
    mutation createUser($user: UserData!) {
  user: CreateUser(data: $user) {
    id
  }
}
    `;
export const UpdateUserDocument = gql`
    mutation updateUser($id: Int!, $user: UserData!) {
  user: UpdateUser(id: $id, data: $user) {
    id
  }
}
    `;

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>;


const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction();

export function getSdk(client: GraphQLClient, withWrapper: SdkFunctionWrapper = defaultWrapper) {
  return {
    allCards(variables?: AllCardsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AllCardsQuery> {
      return withWrapper(() => client.request<AllCardsQuery>(AllCardsDocument, variables, requestHeaders));
    },
    getCard(variables: GetCardQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetCardQuery> {
      return withWrapper(() => client.request<GetCardQuery>(GetCardDocument, variables, requestHeaders));
    },
    deleteCard(variables: DeleteCardMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteCardMutation> {
      return withWrapper(() => client.request<DeleteCardMutation>(DeleteCardDocument, variables, requestHeaders));
    },
    createCard(variables: CreateCardMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateCardMutation> {
      return withWrapper(() => client.request<CreateCardMutation>(CreateCardDocument, variables, requestHeaders));
    },
    updateCard(variables: UpdateCardMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateCardMutation> {
      return withWrapper(() => client.request<UpdateCardMutation>(UpdateCardDocument, variables, requestHeaders));
    },
    sessionCards(variables: SessionCardsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<SessionCardsQuery> {
      return withWrapper(() => client.request<SessionCardsQuery>(SessionCardsDocument, variables, requestHeaders));
    },
    createSession(variables: CreateSessionMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateSessionMutation> {
      return withWrapper(() => client.request<CreateSessionMutation>(CreateSessionDocument, variables, requestHeaders));
    },
    allStudents(variables?: AllStudentsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AllStudentsQuery> {
      return withWrapper(() => client.request<AllStudentsQuery>(AllStudentsDocument, variables, requestHeaders));
    },
    getStudent(variables: GetStudentQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetStudentQuery> {
      return withWrapper(() => client.request<GetStudentQuery>(GetStudentDocument, variables, requestHeaders));
    },
    homeStudent(variables?: HomeStudentQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<HomeStudentQuery> {
      return withWrapper(() => client.request<HomeStudentQuery>(HomeStudentDocument, variables, requestHeaders));
    },
    deleteStudent(variables: DeleteStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteStudentMutation> {
      return withWrapper(() => client.request<DeleteStudentMutation>(DeleteStudentDocument, variables, requestHeaders));
    },
    createStudent(variables: CreateStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateStudentMutation> {
      return withWrapper(() => client.request<CreateStudentMutation>(CreateStudentDocument, variables, requestHeaders));
    },
    updateStudent(variables: UpdateStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateStudentMutation> {
      return withWrapper(() => client.request<UpdateStudentMutation>(UpdateStudentDocument, variables, requestHeaders));
    },
    newActivationCode(variables: NewActivationCodeMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<NewActivationCodeMutation> {
      return withWrapper(() => client.request<NewActivationCodeMutation>(NewActivationCodeDocument, variables, requestHeaders));
    },
    toggleStateStudent(variables: ToggleStateStudentMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ToggleStateStudentMutation> {
      return withWrapper(() => client.request<ToggleStateStudentMutation>(ToggleStateStudentDocument, variables, requestHeaders));
    },
    getSyllabus(variables?: GetSyllabusQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetSyllabusQuery> {
      return withWrapper(() => client.request<GetSyllabusQuery>(GetSyllabusDocument, variables, requestHeaders));
    },
    getStudentSyllabus(variables?: GetStudentSyllabusQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetStudentSyllabusQuery> {
      return withWrapper(() => client.request<GetStudentSyllabusQuery>(GetStudentSyllabusDocument, variables, requestHeaders));
    },
    getChapter(variables: GetChapterQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetChapterQuery> {
      return withWrapper(() => client.request<GetChapterQuery>(GetChapterDocument, variables, requestHeaders));
    },
    getChapterByPos(variables: GetChapterByPosQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetChapterByPosQuery> {
      return withWrapper(() => client.request<GetChapterByPosQuery>(GetChapterByPosDocument, variables, requestHeaders));
    },
    deleteChapter(variables: DeleteChapterMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteChapterMutation> {
      return withWrapper(() => client.request<DeleteChapterMutation>(DeleteChapterDocument, variables, requestHeaders));
    },
    createChapter(variables: CreateChapterMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateChapterMutation> {
      return withWrapper(() => client.request<CreateChapterMutation>(CreateChapterDocument, variables, requestHeaders));
    },
    updateChapter(variables: UpdateChapterMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateChapterMutation> {
      return withWrapper(() => client.request<UpdateChapterMutation>(UpdateChapterDocument, variables, requestHeaders));
    },
    deleteProgress(variables: DeleteProgressMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteProgressMutation> {
      return withWrapper(() => client.request<DeleteProgressMutation>(DeleteProgressDocument, variables, requestHeaders));
    },
    reorderChapter(variables: ReorderChapterMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ReorderChapterMutation> {
      return withWrapper(() => client.request<ReorderChapterMutation>(ReorderChapterDocument, variables, requestHeaders));
    },
    showNotifications(variables?: ShowNotificationsQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ShowNotificationsQuery> {
      return withWrapper(() => client.request<ShowNotificationsQuery>(ShowNotificationsDocument, variables, requestHeaders));
    },
    showStudent(variables: ShowStudentQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<ShowStudentQuery> {
      return withWrapper(() => client.request<ShowStudentQuery>(ShowStudentDocument, variables, requestHeaders));
    },
    saveNotes(variables: SaveNotesMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<SaveNotesMutation> {
      return withWrapper(() => client.request<SaveNotesMutation>(SaveNotesDocument, variables, requestHeaders));
    },
    allUsers(variables?: AllUsersQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<AllUsersQuery> {
      return withWrapper(() => client.request<AllUsersQuery>(AllUsersDocument, variables, requestHeaders));
    },
    getUser(variables: GetUserQueryVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<GetUserQuery> {
      return withWrapper(() => client.request<GetUserQuery>(GetUserDocument, variables, requestHeaders));
    },
    deleteUser(variables: DeleteUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<DeleteUserMutation> {
      return withWrapper(() => client.request<DeleteUserMutation>(DeleteUserDocument, variables, requestHeaders));
    },
    createUser(variables: CreateUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<CreateUserMutation> {
      return withWrapper(() => client.request<CreateUserMutation>(CreateUserDocument, variables, requestHeaders));
    },
    updateUser(variables: UpdateUserMutationVariables, requestHeaders?: Dom.RequestInit["headers"]): Promise<UpdateUserMutation> {
      return withWrapper(() => client.request<UpdateUserMutation>(UpdateUserDocument, variables, requestHeaders));
    }
  };
}
export type Sdk = ReturnType<typeof getSdk>;