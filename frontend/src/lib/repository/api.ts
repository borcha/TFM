import { GraphQLClient } from 'graphql-request'
import { getSdk } from './graphql';
import { getAuthToken, reActivate } from '../auth';

const ENDPOINT = `${process.env.ENDPOINT || '/api'}`;

const client = new GraphQLClient(ENDPOINT);
export default getSdk(client, authWrapper);

async function authWrapper(sdkFunction) {
  try {
    client.setHeader('authorization', `Bearer ${getAuthToken()}`)
    return await sdkFunction();
  } catch (e) {
    if (e.message.startsWith("AuthError")) {
      reActivate();
    } else {
      throw e;
    }
  }
}