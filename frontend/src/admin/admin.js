import '@fortawesome/fontawesome';
import { getAuthToken, teacherMode } from  '~/lib/auth'
import App from '~/admin/pages/App.svelte';
import Login from '~/admin/pages/Login.svelte';
import TimeAgo from 'javascript-time-ago'
import es from 'javascript-time-ago/locale/es'
TimeAgo.addDefaultLocale(es)


teacherMode()
if (getAuthToken() == null) {
  window.app = new Login({
    target: document.body
  });
} else {
  window.app = new App({
    target: document.body
  });
}


