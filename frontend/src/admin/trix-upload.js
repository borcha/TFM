const HOST = `${process.env.CLOUDINARY_UPLOAD_URL}`;
const upload_preset = `${process.env.CLOUDINARY_UPLOAD_PRESET}`;

export default function uploadFileAttachment(attachment) {
  uploadFile(attachment.file, setProgress, setAttributes)

  function setProgress(progress) {
    attachment.setUploadProgress(progress)
  }

  function setAttributes(attributes) {
    attachment.setAttributes(attributes)
  }
}

function uploadFile(file, progressCallback, successCallback) {
  var formData = createFormData(file)
  var xhr = new XMLHttpRequest()

  xhr.open("POST", HOST, true)

  xhr.upload.addEventListener("progress", function(event) {
    var progress = event.loaded / event.total * 100
    progressCallback(progress)
  })

  xhr.addEventListener("load", function(event) {
    if (xhr.status == 200) {
      var result = JSON.parse(xhr.responseText);
      var attributes = {
        url: result.url
      }
      successCallback(attributes)
    }
  })

  xhr.send(formData)
}

function createFormData(file) {
  var data = new FormData()
  data.append("upload_preset", upload_preset)
  data.append("file", file)
  return data
}
