import '@fortawesome/fontawesome';
import { getAuthToken } from  '~/lib/auth'
import App from '~/student/pages/App.svelte';
import Activation from '~/student/pages/Activation.svelte';


if (getAuthToken() == null) {
  window.app = new Activation({
    target: document.body
  });
} else {
  window.app = new App({
    target: document.body
  });
}


