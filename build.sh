#!/bin/bash

if ! [ -x "$(command -v npm)" ]; then
  echo 'Error: No se encuentra comando npm.' >&2
  exit 1
fi

if ! [ -x "$(command -v lein)" ]; then
  echo 'Error: No se encuentra comando lein.' >&2
  exit 1
fi

echo "Building frontend ..."
cd frontend
npm install
npm run build

echo "Building backend ..."
cd ../backend
lein clean
lein uberjar
